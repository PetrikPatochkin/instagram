app.config(function($httpProvider, $stateProvider, $urlRouterProvider){
    $urlRouterProvider.otherwise('/');
    $stateProvider
        .state('Start',{
            url: '/',
            templateUrl: "app/components/start/indexView.html",
            controller: 'StartController'
        })
        .state('Search',{
            url: '/search/:query',
            templateUrl: "app/components/start/searchView.html",
            controller: 'StartController'
        })
        .state('UserPreview',{
            url: '/user/:id',
            templateUrl: "app/components/start/userView.html",
            controller: 'StartController'
        })
        .state('Collage Preview',{
            url: '/user/:id/collage',
            templateUrl: "app/components/collage/previewView.html",
            controller: 'CollageController'
        })
});
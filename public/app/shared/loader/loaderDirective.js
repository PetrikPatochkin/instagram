app.directive('loader', function(){
    return {
        restrict: 'E',
        templateUrl: 'app/shared/loader/loaderView.html',
        controller: function($scope){
            $scope.startLoader = function(){
                $(".container-fluid .content").css('opacity','0.5');
                $('#loader').css('display','block');
            };
            $scope.finishLoader = function(){
                $(".container-fluid .content").css('opacity','1');
                $('#loader').css('display','none');
            }
        }
    }
});
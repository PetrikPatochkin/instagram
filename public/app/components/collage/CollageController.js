app.controller('CollageController',function($scope,$http,$state,$stateParams){
    $scope.id = $stateParams.id;

    $scope.sizeOptions = [
        { name: 'Случайно', value: '0' },
        { name: '3', value: '3' },
        { name: '4', value: '4' },
        { name: '5', value: '5' },
        { name: '6', value: '6' },
        { name: '7', value: '7' },
        { name: '8', value: '8' },
        { name: '9', value: '9' },
        { name: '10', value: '10' },
        { name: '11', value: '11' },
        { name: '12', value: '12' },
        { name: '13', value: '13' },
        { name: '14', value: '14' },
        { name: '15', value: '15' },
        { name: '16', value: '16' }
    ];
    $scope.collage_size = {type : $scope.sizeOptions[0].value};


    $scope.loadUser = function(id){
        $scope.startLoader();
        $http.get('/finduser',{params:{id:id}})
            .then(function(data){
                $scope.user = data.data;
                $scope.finishLoader();
            });
        $http.get('/getusermedia',{params:{id:id}})
            .then(function(data){
                $scope.media = data.data;
                $scope.finishLoader();
            });
    };

    $scope.createCollage = function(user_id, size){
        $scope.startLoader();
        $http.get('/collage/create',{params:{id:user_id,size:size}})
            .then(function(data){
                $scope.finishLoader();
                $('#result-link').attr('href',data.data.url);
                $('#result-link').click();
            });
    }
});
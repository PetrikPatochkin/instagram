app.controller('StartController',function($scope,$http,$state,$stateParams){
    $scope.authenticated = undefined;
    $scope.searched = [];
    $scope.params = $stateParams;

    $scope.isAuthenticated = function(){
        return $scope.authenticated;
    };

    $scope.getMyInfo = function(){
        $scope.startLoader();
        $http.get('/self/info')
            .then(function(data){
                $scope.authenticated = true;
                $scope.authInfo = data.data;
                $scope.finishLoader();
            })
            .catch(function(){
                $scope.authenticated = false;
                $scope.finishLoader();
            });
    };
    $scope.searchSubmit = function(query){
        $state.go('Search',{query: query});
    };
    $scope.searchUsers = function(query){
        $scope.startLoader();
        $http.get('/search/users',{params:{q:query}})
            .then(function(data){
                $scope.searched = data.data;
                $scope.finishLoader();
            });
    };

    $scope.selectUser = function(user){
        $scope.selected = user;
        $state.go('UserPreview',{id: $scope.selected.id});
    };

    $scope.loadUser = function(id){
        $scope.startLoader();
        $http.get('/finduser',{params:{id:id}})
            .then(function(data){
                $scope.selected = data.data;
                $scope.finishLoader();
            });
        $http.get('/getusermedia',{params:{id:id}})
            .then(function(data){
                $scope.selectedMedia = data.data;
                $scope.finishLoader();
            });
    }
});
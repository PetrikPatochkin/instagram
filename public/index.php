<?php
header('Content-Type: text/html; charset=utf-8');

require_once "../vendor/autoload.php";

$bootstrap = new App\Core\Bootstrap();
$bootstrap->run();
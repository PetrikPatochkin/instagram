<?php namespace App\Core\Image\Imagine;

use App\Core\Image\ImageInterface;
use Imagine as VImagine;
use Imagine\GD\Image as VImage;

class ImageAdapter implements ImageInterface{
    protected $image = NULL;

    public function __construct($image){
        $this->image = $image;
    }

    public function resize($width, $height){
        return $this->image->resize(new VImagine\Image\Box($width, $height));
    }

    public function paste($inserted_image, $x, $y){
        return $this->image->paste($inserted_image, new VImagine\Image\Point($x, $y));
    }

    public function save($path){
        return $this->image->save($path);
    }

    public function show($format){
        return $this->image->show($format);
    }
}

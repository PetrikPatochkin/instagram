<?php namespace App\Core\Image\Imagine;

use App\Core\Image\Imagine\ImageAdapter;
use Imagine as VImagine;

class ImagineAdapter{

    protected $imagine = NULL;

    public function __construct(VImagine\Image\ImagineInterface $imagine){
        $this->imagine = $imagine;
    }

    public function open($path){
        return new ImageAdapter($this->imagine->open($path));
    }

    public function create($width, $height){
        return new ImageAdapter($this->imagine->create(new VImagine\Image\Box($width, $height)));
    }
}

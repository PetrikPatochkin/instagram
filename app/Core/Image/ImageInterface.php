<?php namespace App\Core\Image;

interface ImageInterface{

    public function resize($width, $height);

    public function paste($inserted_image, $x, $y);

    public function save($path);

    public function show($format);
}
<?php namespace App\Core\Image\GD\Formats;

class Gif implements FormatInterface{

    public function create($path){
        return imagecreatefromgif($path);
    }

    public function save($image, $path){
        return imagegif($image, $path);
    }

    public function show($image){
        header('Content-Type: image/jpeg');
        imagegif($image, NULL);
    }
}
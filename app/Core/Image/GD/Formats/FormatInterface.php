<?php namespace App\Core\Image\GD\Formats;


interface FormatInterface {
    public function create($path);

    public function save($image, $path);

    public function show($path);
}
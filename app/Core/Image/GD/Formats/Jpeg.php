<?php namespace App\Core\Image\GD\Formats;

class Jpeg implements FormatInterface{

    public function __construct(){}

    public function create($path){
        return imagecreatefromjpeg($path);
    }

    public function save($image, $path){
        return imagejpeg($image, $path);
    }

    public function show($image){
        header('Content-Type: image/jpeg');
        imagejpeg($image, NULL);
    }
}
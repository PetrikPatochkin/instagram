<?php namespace App\Core\Image\GD\Formats;

class Png implements FormatInterface{

    public function create($path){
        return imagecreatefrompng($path);
    }

    public function save($image, $path){
        return imagepng($image, $path);
    }

    public function show($image){
        header('Content-Type: image/jpeg');
        imagepng($image, NULL);
    }
}
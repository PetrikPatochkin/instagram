<?php namespace App\Core\Image\GD;

use App\Core\Image\ImageInterface;

class Image implements ImageInterface{

    protected $image = NULL;
    protected $typeStrategy = NULL;
    protected $info = NULL;


    public function createEmpty($width, $height){
        $this->info = [$width, $height];
        $this->typeStrategy = new Formats\Jpeg();
        $this->image = imagecreatetruecolor($width, $height);
        return $this;
    }

    public function createFromPath($image_path){
        // Get image information
        $this->info = getimagesize($image_path);
        // Choose strategy depending on image type
        switch($this->info['mime']){
            case "image/jpeg":
                $this->typeStrategy = new Formats\Jpeg();
                break;
            case "image/png":
                $this->typeStrategy = new Formats\Png();
                break;
            case "image/gif":
                $this->typeStrategy = new Formats\Gif();
                break;
            default:
                die("Cannot create image from path \"".$image_path."\"");
        }
        // Create image
        $this->image = $this->typeStrategy->create($image_path);
        return $this;
    }

    public function resize($new_width, $new_height){
        list($width, $height) = $this->info;
        $thumb = new Image();
        $thumb->createEmpty($new_width, $new_height);
        // Resize
        imagecopyresized($thumb->image, $this->image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

        return $thumb;
    }

    public function paste($inserted_image, $x, $y){
        list($inserted_width, $inserted_height) = $inserted_image->info;
        imagecopy($this->image, $inserted_image->image, $x, $y, 0, 0, $inserted_width, $inserted_height);
        return $this;
    }

    public function save($path){
        return $this->typeStrategy->save($this->image, $path);
    }

    public function show($format){
        $this->typeStrategy->show($this->image);
    }
}


<?php namespace App\Core\Image\GD;

use App\Core\Image\ImageMakerInterface;
use App\Core\Image\GD\Image;

class ImageMaker implements ImageMakerInterface{

    public function __construct(){
    }

    public function create($width, $height){
        $image = new Image();
        return $image->createEmpty($width, $height);
    }

    public function open($path){
        $image = new Image();
        return $image->createFromPath($path);
    }
}

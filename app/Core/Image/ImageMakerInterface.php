<?php namespace App\Core\Image;

interface ImageMakerInterface{

    public function open($path);

    public function create($width, $height);
}
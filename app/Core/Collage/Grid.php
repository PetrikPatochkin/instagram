<?php namespace App\Core\Collage;

use App\Core\Collage\Section;

class Grid {
    protected $_images_cnt;
    protected $_cell_length;
    protected $_collage_max_length;

    public $_width;
    public $_height;

    protected $_sections;

    public function __construct($cell_length=320, $collage_max_length=1000, $images_cnt=1){
        $this->_images_cnt = $images_cnt;
        $this->_cell_length = $cell_length;
        $this->_collage_max_length = $collage_max_length;

        $this->_sections = [];
    }

    public function build(){
        $positions_left = $this->_images_cnt;
        $section_new = [
            'size_cell' => 1,
            'section_repeated' => 0, //2 times per section
            'direction' => 'left' // left,bottom,right,top
        ];
        $build_dot = [
            'index' => 0,
            'x' => 0,
            'y' => 0
        ];

        while( $positions_left > 0.5 * $section_new['size_cell']){
            $section = new Section($section_new['size_cell'], $section_new['direction'], $this->_cell_length);
            $section->setCoordinates($build_dot['x'],$build_dot['y']);
            // Put number of images positions to section
            $positions_in_section = ($positions_left < $section_new['size_cell'])?$positions_left:$section_new['size_cell'];
            $section->setPositionsContentCnt($positions_in_section);
            // Update free images
            $positions_left -= $positions_in_section;
            $this->_sections[] = $section;

            //---- Check if coordinates became < 0 and offset all grid
            $build_dot = $this->offsetGridAndDotIfNeeded($build_dot);
            //---- Recalculate width and height
            $this->gridSizeRecalculate($build_dot, $section_new['direction'],$section_new['size_cell'] );

            //---- Next building parameters
            // change new section direction
            $section_new['direction'] = $this->nextDirection($section_new['direction']);
            $section_new['section_repeated']++;
            // Section size repeats twice
            if($section_new['section_repeated'] == 2){
                $section_new['size_cell']++;
                $section_new['section_repeated'] = 0;
            }
            // increment dot index
            $build_dot['index']++;
            $new_dot = $this->getNextBuildDot($build_dot, $section_new['direction'], $section_new['size_cell']);
            $build_dot['x'] = $new_dot['x'];
            $build_dot['y'] = $new_dot['y'];
        }
        // Add lost positions that will not take place in new section
        $last_section = end($this->_sections);
        if($positions_left > 0){
            $last_section->addPositionsContentCnt($positions_left);
            $positions_left = 0;
        }
        // Need to resize last section to achieve square positions
        $this->resizeLastSectionAndGrid($last_section);
    }

    protected function resizeLastSectionAndGrid($section){
        $section_info = $section->getInfo();
        switch($section_info['direction']){
            case 'bottom':
                $new_height = round($section_info['width'] / $section_info['positions'],0);
                $section->setHeight($new_height);
                $this->_height += $new_height - $section_info['height'];
                break;
            case 'right':
                $new_width = round($section_info['height'] / $section_info['positions'],0);
                $section->setWidth($new_width);
                $this->_width += $new_width - $section_info['width'];
                break;
            case 'top':
                $new_height = round($section_info['width'] / $section_info['positions']);
                $section->setHeight($new_height);
                $this->_height += $new_height - $section_info['height'];

                $new_y = $section_info['height'] - $new_height;
                $section->setCoordinates($section_info['x'], $new_y);

                $this->offsetGridToStart(['x'=>$section_info['x'],'y'=>$new_y]);
                break;
            case 'left':
                $new_width = round($section_info['height'] / $section_info['positions']);
                $section->setWidth($new_width);
                $this->_width += $new_width - $section_info['width'];

                $new_x = $section_info['width'] - $new_width;
                $section->setCoordinates($new_x, $section_info['y']);

                $this->offsetGridToStart(['x'=>$new_x,'y'=>$section_info['y']]);
                break;
        }
    }

    protected function offsetGridAndDotIfNeeded($last_built_dot){
        if($last_built_dot['x']<0){
            foreach($this->_sections as $sect){
                $sect->offset('right', -$last_built_dot['x']);
            }
            $last_built_dot['x'] = 0;
        }
        if($last_built_dot['y']<0){
            foreach($this->_sections as $sect){
                $sect->offset('bottom', -$last_built_dot['y']);
            }
            $last_built_dot['y'] = 0;
        }
        return $last_built_dot;
    }

    protected function offsetGridToStart($current_point){
        foreach($this->_sections as $sect){
            $sect->offset('right', -$current_point['x']);
        }
        foreach($this->_sections as $sect){
            $sect->offset('bottom', -$current_point['y']);
        }
        $current_point['x'] = 0;
        $current_point['y'] = 0;
        return $current_point;
    }

    protected function gridSizeRecalculate($current_dot, $last_direction, $last_size_cell){
        switch($last_direction){
            case 'bottom':
            case 'top':
                $this->_width = $last_size_cell * $this->_cell_length;;
                $this->_height = ($last_size_cell+1) * $this->_cell_length;
                break;
            case 'right':
            case 'left':
                $this->_width = $last_size_cell * $this->_cell_length;;
                $this->_height = $last_size_cell * $this->_cell_length;
                break;
        }
    }

    protected function nextDirection($direction){
        $directions = ['left','bottom','right','top'];
        $current = array_search($direction, $directions);
        return $directions[($current+1)%(count($directions))];
    }

    protected function getNextBuildDot($current_dot, $direction, $size_cell){
        $x = $current_dot['x'];
        $y = $current_dot['y'];
        switch($direction){
            case 'bottom':
                $y += $size_cell * $this->_cell_length;
                break;
            case 'right':
                $x += ($size_cell-1) * $this->_cell_length;
                $y -= ($size_cell-1) * $this->_cell_length;
                break;
            case 'top':
                $x -= ($size_cell-1) * $this->_cell_length;
                $y -= ($size_cell-1) * $this->_cell_length;
                break;
            case 'left':
                $x -= ($size_cell-2) * $this->_cell_length;
                break;
        }
        return ['x'=>$x, 'y'=>$y];
    }

    public function fillImages($images){
        $this->_images_cnt = count($images);
        $this->build();
        foreach($this->_sections as $section){
            $section->fillImages($images);
        }
    }

    public function render($imagine, $collage){
        foreach($this->_sections as $section){
            $section->render($imagine, $collage);
        }
    }

    public function echoSections(){
        foreach($this->_sections as $section){
            $point = $section->getInfo();
            echo "<pre>";
            print_r($point);
            echo "</pre>";
        }
    }

    public function getSize(){
        return [
            'width' => $this->_width,
            'height'=> $this->_height
        ];
    }
}
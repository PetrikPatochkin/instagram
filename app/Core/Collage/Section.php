<?php namespace App\Core\Collage;


class Section {
    protected $_width_cell;
    protected $_height_cell;
    protected $_direction;

    protected $_images_content_cnt;
    protected $_images;

    protected $_x;
    protected $_y;
    protected $_width;
    protected $_height;

    public function __construct($size, $direction, $cell_length){
        $this->_direction = $direction;
        $this->_width_cell = ($direction=='top'||$direction=='bottom') ? $size : 1;
        $this->_height_cell = ($direction=='left'||$direction=='right') ? $size : 1;

        $this->_width = $this->_width_cell * $cell_length;
        $this->_height = $this->_height_cell * $cell_length;

        $this->_images = [];
    }

    public function setCoordinates($x, $y){
        $this->_x = $x;
        $this->_y = $y;
    }

    public function getInfo(){
        return [
            'x'=>$this->_x,
            'y'=>$this->_y,
            'width'=>$this->_width,
            'height'=>$this->_height,
            'width_cell'=>$this->_width_cell,
            'height_cell'=>$this->_height_cell,
            'positions'=>$this->_images_content_cnt,
            'direction'=>$this->_direction,
            'images' => $this->_images
        ];
    }

    public function setWidth($width){
        $this->_width = $width;
    }

    public function setHeight($height){
        $this->_height = $height;
    }

    public function setPositionsContentCnt($cnt){
        $this->_images_content_cnt = $cnt;
        switch($this->_direction){
            case "top":
            case "bottom":
                $this->_width_cell = $cnt;
                break;
            case "left":
            case "right":
                $this->_height_cell = $cnt;
                break;
        }
    }

    public function addPositionsContentCnt($cnt){
        $this->_images_content_cnt += $cnt;
        switch($this->_direction){
            case "top":
            case "bottom":
                $this->_width_cell += $cnt;
                break;
            case "left":
            case "right":
                $this->_height_cell += $cnt;
                break;
        }
    }

    public function offset($direction, $length){
        if($direction == 'right'){
            $this->_x += $length;
        }
        if($direction == 'bottom'){
            $this->_y += $length;
        }
    }

    public function fillImages(&$images){
        $x = $this->_x;
        $y = $this->_y;
        $width = round($this->_width / $this->_width_cell,0);
        $height = round($this->_height / $this->_height_cell,0);
        for($i=0; $i<$this->_images_content_cnt; $i++){
            $image = [
                'path' => array_shift($images),
                'x' => $x,
                'y' => $y,
                'width' => $width,
                'height' => $height,
            ];
            array_push($this->_images, $image);
            switch($this->_direction){
                case "top":
                case "bottom":
                    $x += $width;
                    break;
                case "left":
                case "right":
                    $y += $height;
                    break;
            }
        }
    }

    public function render($imagine, $collage){
        foreach($this->_images as $image){
            $image_cell = $imagine->open($image['path'])->resize($image['width'], $image['height']);
            $collage->paste($image_cell, $image['x'], $image['y']);
        }
    }

}
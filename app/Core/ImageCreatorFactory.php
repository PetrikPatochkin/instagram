<?php namespace App\Core;

use Imagine\GD\Imagine;
use App\Core\Image\Imagine\ImagineAdapter;
use App\Core\Image\GD\ImageMaker;

class ImageCreatorFactory {
    public static function create($type){
        switch ($type){
            case "imagine":
                return new ImagineAdapter(new Imagine());
            case "GD":
                return new ImageMaker();
            default:
                return NULL;
        }
    }
}
<?php namespace App\Core;

use App\Core\Collage\Grid;
use Imagine;

class Collage{

    /*public function make(){
        $imagine = ImageCreatorFactory::create('GD');

        $collage = $imagine->create(1600, 1600);

        // starting coordinates (in pixels) for inserting the first image
        $x = 0;
        $y = 0;

        for ($i=0; $i<160; $i++) {
            // open photo
            $photo = $imagine->open('images/test.jpg')->resize(400,400);

            // paste photo at current position
            $collage->paste($photo, $x, $y);

            // move position by 30px to the right
            $x += 400;

            if ($x >= 1600) {
                // we reached the right border of our collage, so advance to the
                // next row and reset our column to the left.
                $y += 400;
                $x = 0;
            }

            if ($y >= 1600) {
                break; // done
            }
        }

        $collage->save('images/res.jpg');
        $collage->show('jpg');
    }*/


    public function test(){
        $imagine = Core\ImageCreatorFactory::create('GD');
        $collage = $imagine->create(1600, 1600);
        $photo = $imagine->open('images/test.jpg')->resize(100,100);
        $collage->paste($photo, 0, 0);
        $collage->show('jpg');
        $collage->save('images/res.jpg');
    }
}
<?php namespace App\Core;

class Session {

    public function __construct(){
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    public function set($key, $value){
        $_SESSION[$key] = $value;
    }

    public function has($key){
        return isset($_SESSION[$key]);
    }

    public function get($key){
        return $this->has($key)? $_SESSION[$key] : NULL;
    }
}
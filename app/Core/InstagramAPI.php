<?php namespace App\Core;


class InstagramAPI {
    protected $_request;
    protected $_config;

    public function __construct(){
        $this->_config = new Config();
        $this->_request = new Request($this->_config);
    }

    public function getAuthLink(){
        $url = $this->_config->get('API.base_url')
                .'/oauth/authorize/?'
                .'client_id='.$this->_config->get('API.client_id')
                .'&redirect_uri='.$this->_config->get('API.redirect_uri')
                .'&response_type=code';
        return $url;
    }

    public function getAuthInfo($code){
        $data = [
            'client_id'=>$this->_config->get('API.client_id'),
            'client_secret'=>$this->_config->get('API.client_secret'),
            'grant_type'=>'authorization_code',
            'redirect_uri'=>$this->_config->get('API.redirect_uri'),
            'code'=>$code,
        ];
        $result = $this->_request->curl($this->_config->get('API.base_url').'/oauth/access_token',$data);
        return $result;
    }

    public function searchUsers($query){
        $result = $this->_request->send('users/search',['q'=>$query]);
        $data = json_decode($result);
        if($data->meta->code == '200')
            return $data->data;
        else
            die($data->meta->error_message);
    }

    public function findUser($id){
        $result = $this->_request->send('users/'.$id);
        $data = json_decode($result);
        if($data->meta->code == '200')
            return $data->data;
        else
            die($data->meta->error_message);
    }

    public function userMedia($id){
        $result = $this->_request->send('users/'.$id.'/media/recent');
        $data = json_decode($result);
        if($data->meta->code == '200')
            return $data->data;
        else
            die("Error:".$data->meta->error_message);
    }

    public function myFeed(){
        $result = $this->_request->send('users/self/feed');
        $data = json_decode($result);
        if($data->meta->code == '200')
            return $data->data;
        else
            die("Error:".$data->meta->error_message);
    }
}
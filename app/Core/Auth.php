<?php namespace App\Core;

use App\Core\Session;

class Auth {
    protected static $_instance;
    protected static $_storage;
    protected static $_auth;

    public static function setInstance(){
        if(null===self::$_instance){
            self::$_instance = new self();
            self::$_storage = new Session();
        }
    }

    protected static function _setAuth($data){
        self::setInstance();
        self::$_storage->set('auth',$data);
    }
    public static function _getAuth(){
        self::setInstance();
        self::$_auth = self::$_storage->has('auth')?self::$_storage->get('auth'):[];
        return self::$_auth;
    }

    public static function isAuthenticated(){
        $auth = self::_getAuth();
        return isset($auth['access_token']);
    }

    public static function authorize($data){
        self::_setAuth($data);
    }

    public static function getUserInfo(){
        $auth = self::_getAuth();
        return isset($auth['user'])?$auth['user']:NULL;
    }

    public static function getToken(){
        $auth = self::_getAuth();
        return isset($auth['access_token'])?$auth['access_token']:NULL;
    }
}
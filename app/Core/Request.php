<?php namespace App\Core;


class Request {
    protected $_url;
    protected $_config;

    public function __construct($config){
        $this->_config = $config;
    }

    public function send($url, $data=[], $options=[]){
        $options['http']['ignore_errors'] = true;
        $this->_makeUrl($url, $data);
        return @file_get_contents($this->_url,false,stream_context_create($options));
    }

    protected function _makeUrl($url, $data){
        $this->_url = $this->_config->get('API.url');
        $this->_url .= '/'.$url;
        $this->_url .= '?';
        $data = $this->_addSignature($data);
        foreach ($data as $key=>$value){
            $this->_url .= $key."=".$value. "&";
        }
        rtrim($this->_url, '&');
        return $this->_url;
    }

    protected function _addSignature($data){
        if(Auth::isAuthenticated()){
            $data['access_token'] = Auth::getToken();
        } else {
            $data['client_id'] = $this->_config->get('API.client_id');
        }
        return $data;
    }

    public function curl($url, $data=[]){
        try{
            $ch = curl_init($url);
            curl_setopt($ch,CURLOPT_POST, count($data));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

            curl_setopt($ch,CURLOPT_HEADER, 0);
            curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 15);
            curl_setopt($ch,CURLOPT_FRESH_CONNECT, 1);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER , 1);
            curl_setopt($ch,CURLOPT_POSTFIELDS, http_build_query($data));
            $result = curl_exec($ch);
            curl_close($ch);
        }catch(\Exception $e){
            die($e->getMessage());
        }
        return $result;
    }

}
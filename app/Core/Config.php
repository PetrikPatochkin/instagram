<?php namespace App\Core;


class Config {
    protected $file = "../app/config.php";
    protected $config = [];

    public function __construct(){
        $this->_parse(require($this->file));
    }

    protected function _parse(Array $data, $path = ''){
        foreach($data as $key => $value){
            if(is_array($value)){
                $this->_parse($value, $path.$key.".");
            } else {
                $this->config[$path.$key] = $value;
            }
        }
    }

    public function get($key){
        return isset($this->config[$key]) ? $this->config[$key]: NULL;
    }
}
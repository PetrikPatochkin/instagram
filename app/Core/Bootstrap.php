<?php namespace App\Core;

class Bootstrap {
    protected $_routes;
    protected $_make;

    public function run(){
        $this->_routes = require_once('../app/routes.php');

        $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
        $routes = $this->_routes;
        $this->_make = isset($routes[$uri_parts[0]])
                        ? $routes[$uri_parts[0]]
                        : ($routes['default']
                            ? $routes['default']
                            : NULL);
        $this->_process();
    }

    protected function _process(){
        if($this->_make===NULL) die('This page doesn\'t exist');
        $parts = explode('@', $this->_make);
        $class_name = "App\\Controllers\\".$parts[0];
        $ctrl = new $class_name;
        $res = call_user_func(array($ctrl, $parts[1]));
        if ($res) echo $res;
    }
}
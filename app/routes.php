<?php

return [
    '/'             => "Index@index",
    '/search/users' => "Index@search_users",
    '/finduser'     => "Index@find_user",
    '/getusermedia' => "Index@user_media",

    '/auth'         => "Login@redirect",
    '/auth/completed'=>"Login@save",

    '/self/info'    => "Index@getMyInfo",

    '/collage/create'=> "Collage@create",

    'default'       => "Index@index",
];
<?php namespace App\Controllers;

use App\Core\Auth;
use App\Core\InstagramAPI;
use App\Core\Request;
use App\Core\Session;

class Login {
    protected $api;
    public function __construct(){
        $this->api = new InstagramAPI();
    }

    public function redirect(){
        header("Location: ".$this->api->getAuthLink());
    }

    public function save(){
        $code = $_GET['code'];
        $instagram_response = $this->api->getAuthInfo($code);
        Auth::authorize(json_decode($instagram_response, true));
        header("Location: /");
    }
}
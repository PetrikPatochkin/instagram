<?php namespace App\Controllers;

use App\Core\ImageCreatorFactory;
use App\Core\InstagramAPI;
use App\Core\Collage as CollageCreator;

class Collage {
    protected $api;
    public function __construct(){
        $this->api = new InstagramAPI();
    }

    public function create(){
        $id = $_GET['id'];
        $size = $_GET['size'];

        if($id !== 'self')
            $images_dirty = $this->api->userMedia($id);
        else
            $images_dirty = $this->api->myFeed();

        $images = [];
        foreach($images_dirty as $image_dirty){
                $images[] = $image_dirty->images->low_resolution->url;
        }
        shuffle($images);
        if($size==0) $size = rand(3,16);
        $images = array_slice($images, 0, $size);

        $grid = new CollageCreator\Grid();
        $grid->fillImages(
            $images
        );
        $gridSize = $grid->getSize();

        $imagine = ImageCreatorFactory::create('GD');
        $collage = $imagine->create($gridSize['width'], $gridSize['height']);

        $grid->render($imagine, $collage);

        $filename = 'img/temp-'.md5(time()).'.jpg';

        $collage->save($filename);
        return json_encode(['url'=>'/'.$filename]);
    }

}
<?php namespace App\Controllers;

use App\Core\Auth;
use App\Core\InstagramAPI;
use App\Core\Session;

class Index {
    protected $api;
    public function __construct(){
        $this->api = new InstagramAPI();
    }

    public function index(){

        $session = new Session();
        $session->set('test',['igor'=>['gigoe'=>'123']]);
        print_r($session->get('test'));
    }

    public function getMyInfo(){
        if(Auth::isAuthenticated()){
            return json_encode(Auth::getUserInfo());
        }
        return header("HTTP/1.1 401 Unauthorized");;
    }

    public function search_users(){
        $q = $_GET['q'];

        $result = $this->api->searchUsers($q);
        return json_encode($result);
    }

    public function find_user(){
        $id = $_GET['id'];
        $result = $this->api->findUser($id);
        return json_encode($result);
    }

    public function user_media(){
        $id = $_GET['id'];
        if($id !== 'self')
            $result = $this->api->userMedia($id);
        else
            $result = $this->api->myFeed();
        return json_encode($result);
    }
}